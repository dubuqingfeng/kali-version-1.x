#About Kali
#
#
FROM kalilinux/kali

MAINTAINER Dubu Qingfeng <1135326346@qq.com>

RUN rm /etc/apt/sources.list

ADD sources.list /etc/apt/sources.list

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update && apt-get -y dist-upgrade && apt-get clean

CMD ["/bin/bash"]
